var geocoder = new google.maps.Geocoder();
var address = "San Diego, CA, 92111"; //Add 

var latitude;
var longitude;
var color = "#d13138"; //Set your tint color. Needs to be a hex value.

function getGeocode() {
	geocoder.geocode( { 'address': address}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
    		latitude = 5.358840
			longitude = -3.987378; 
			initGoogleMap();
    	} 
	});
}

function initGoogleMap() {
	var styles = [
	    {
	      stylers: [
	        { saturation: -100 }
	      ]
	    }
	];
	
	var options = {
		mapTypeControlOptions: {
			mapTypeIds: ['Styled']
		},
		center: new google.maps.LatLng(latitude, longitude),
		zoom: 17,
		scrollwheel: false,
		navigationControl: false,
		mapTypeControl: false,
		zoomControl: true,
		disableDefaultUI: true,	
		mapTypeId: 'Styled'
	};
	var div = document.getElementById('googleMap');
	var map = new google.maps.Map(div, options);
	marker = new google.maps.Marker({
	    map:map,
	    draggable:false,
	    animation: google.maps.Animation.DROP,
      icon:'http://i.imgur.com/KupWAzW.png',
	    position: new google.maps.LatLng(latitude,longitude)
	});
	var styledMapType = new google.maps.StyledMapType(styles, { name: 'Styled' });
	map.mapTypes.set('Styled', styledMapType);
	
	var infowindow = new google.maps.InfoWindow({
		content: "<div class='iwContent'>Z Fit Spa<br>+225 22 520 835<br> <a href='http//www.zfitspa.com'>www.zfitspa.com</a></div>"
	});
	google.maps.event.addListener(marker, 'click', function() {
	    window.location = "http://zfitspa.com";
	});
	google.maps.event.addListener(marker, 'mouseover', function() {
	    infowindow.open(map,marker);
	});
		
	
	bounds = new google.maps.LatLngBounds(
	  new google.maps.LatLng(-84.999999, -179.999999), 
	  new google.maps.LatLng(84.999999, 179.999999));

	rect = new google.maps.Rectangle({
	    bounds: bounds,
	    fillColor: color,
	    fillOpacity: 0.2,
	    strokeWeight: 0,
	    map: map
	});

	var listener = google.maps.event.addListener(map, "idle", function() { 
		$('#map-banner').show();
		$("#map-header").fitText(1.2, { minFontSize: '20px', maxFontSize: '400px'});
	  google.maps.event.removeListener(listener); 
	});

}
google.maps.event.addDomListener(window, 'load', getGeocode);